isEmpty(VNR_RW_INCLUDED) {
# include this as .PRI rather than as dynamic library, if you want string lliterals translated.
VNR_RW_INCLUDED = 1
INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/CommonAsterixReader.cpp \
    $$PWD/DataIOLib.cpp \
    $$PWD/PcapReader.cpp \
    $$PWD/VnrReader.cpp \
    $$PWD/VnrWriter.cpp

HEADERS  += \
    $$PWD/CommonAsterixReader.h \
    $$PWD/DataIOLib.h \
    $$PWD/PcapReader.h \
    $$PWD/VnrReader.h \
    $$PWD/VnrWriter.h \
    $$PWD/VnrRWTypes.h \
    $$PWD/AsterixFileInfo.h
}

