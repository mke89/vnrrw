#ifndef VNRRWTYPES_H
#define VNRRWTYPES_H

#include <QDateTime>
#include <QHostAddress>
#include "AsterixFileInfo.h"

namespace VnrRW
{
    constexpr char Preamble[] = "VNR.BIN   ";

    using RecordTime = AsterixFileInfo::RecordTime;
    using RecordData = AsterixFileInfo::RecordData;
    using SenderAddress = QHostAddress;
    using SenderPort = unsigned int;
    using RecordType = std::tuple<RecordTime, RecordData, SenderAddress, SenderPort>;


    enum VnrRWParams
    {
        PREAMBLE_LEN            = sizeof(Preamble) - 1,
        DESCR_FIELD_SIZE        = 4,
        FILE_WRITE_TIME_SIZE    = 8,

        DELIMETER_LEN           = 2,
        DATA_LENGTH_LEN         = 4,
        SENDER_ADDR_LEN         = 4,
        SENDER_PORT_LEN         = 2,
        SEND_TIME_LEN           = 4,

        RECORD_HEADER_LEN       = DELIMETER_LEN +
                                  DATA_LENGTH_LEN +
                                  SENDER_ADDR_LEN +
                                  SENDER_PORT_LEN +
                                  SEND_TIME_LEN,

        IP_ADDR_OFFSET          = DELIMETER_LEN + DATA_LENGTH_LEN,
        PORT_OFFSET             = IP_ADDR_OFFSET + SENDER_ADDR_LEN,
        RECORD_TIME_OFFSET      = PORT_OFFSET + SENDER_PORT_LEN,
    };
}

#endif // VNRRWTYPES_H
