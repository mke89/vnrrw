#ifndef VNRWRITER_H
#define VNRWRITER_H

#include <QString>
#include <QPair>
#include <QDateTime>
#include <QHostAddress>
#include <stdio.h>

#include "VnrRWTypes.h"

class VnrWriter
{
    public:
        VnrWriter();
        VnrWriter(QString file, QString description);
        void setFile(QString file);
        void setDescription(QString description);
        bool fileOpened();
        bool open();
        void flush();
        void close();
        QDateTime getCreationTime();
        void setCreationTime(QDateTime creationDateTime);
        bool write(const QList<std::tuple<VnrRW::RecordTime, VnrRW::RecordData, VnrRW::SenderAddress,
                                          VnrRW::SenderPort>> &data, bool flush = false);
        ~VnrWriter();

    private:
        QString fileName;
        QString description;
        FILE *fileHandler;
        bool firstRecord;
        QDateTime creationTime;
        VnrRW::RecordTime firstRecordTime;
        char header[VnrRW::RECORD_HEADER_LEN];

        bool writePreamble();
        bool writeRecordTime();
        bool writeDescription();
        bool writeFileHeader();
        bool convertValueToHex(char* hexString, size_t stringLen, size_t fieldLen, unsigned long long value);
        void makeRecordHeader(char *headerBuffer, const VnrRW::RecordTime &recordTime,
                                                  const VnrRW::RecordData &recordData,
                                                  const VnrRW::SenderAddress &senderAddress,
                                                  VnrRW::SenderPort senderPort);
};

#endif // VNRWRITER_H
