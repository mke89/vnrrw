#ifndef DATAIOLIB_H
#define DATAIOLIB_H

#include <QList>
#include <QPair>
#include <QDateTime>
#include <QByteArray>
#include <QString>
#include "AsterixFileInfo.h"


class DataIOLib
{
    private:
        static int getLastRecordOffetMsecs(FILE *fileDesc, char *stringBuffer, int bufferSize);

    public:
        DataIOLib();
        static QList<QPair<QDateTime, QByteArray>> readFile(QString file, int recordTimeshift_ms = 0, int cutFirstBytes = 0);
        static QList<QPair<QDateTime, QByteArray>> readDir(QString dir, int recordTimeshift_ms = 0);
        static AsterixFileInfo::FileDataInfo getFileDataInfo(QString file, int stringsCount = 100, int cutFirstBytes = 0);
        static QDateTime getInitialDateTime(char *dateTimeString);
        static int convertStringViewToBin(char *inputString, char *outputString);
};

#endif // DATAIOLIB_H
