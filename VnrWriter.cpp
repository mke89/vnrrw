#include "VnrWriter.h"
#include <string>

VnrWriter::VnrWriter(): fileHandler(nullptr)
{
}

VnrWriter::VnrWriter(QString file, QString description):
    fileName(file),
    description(description),
    fileHandler(nullptr),
    firstRecord(true),
    creationTime(QDateTime()),
    firstRecordTime(QDateTime())
{
    open();
}

QDateTime VnrWriter::getCreationTime()
{
    return creationTime;
}

void VnrWriter::setCreationTime(QDateTime creationDateTime)
{
    creationTime = creationDateTime;
}

void VnrWriter::setFile(QString file)
{
    fileName = file;
}

void VnrWriter::setDescription(QString description)
{
    this->description = description;
}

bool VnrWriter::fileOpened()
{
    return fileHandler;
}

bool VnrWriter::open()
{
    if((!fileName.endsWith(".vnr")) || fileName.isEmpty())
    {
        return false;
    }

    if(fileHandler)
    {
        return false;
    }

    fileHandler = fopen(fileName.toStdString().c_str(), "wb");
    if(!fileHandler)
    {
        return false;
    }

    firstRecord = true;
    creationTime = QDateTime::currentDateTime();

    return true;
}

bool VnrWriter::writeFileHeader()
{
    if(!writePreamble())
    {
        return false;
    }

    if(!writeRecordTime())
    {
        return false;
    }

    if(!writeDescription())
    {
        return false;
    }

    return true;
}

void VnrWriter::flush()
{
    if(!fileHandler)
    {
        return;
    }

    fflush(fileHandler);
}

void VnrWriter::close()
{
    if(!fileHandler)
    {
        return;
    }

    fclose(fileHandler);
    fileHandler = nullptr;
}

VnrWriter::~VnrWriter()
{
    close();
}

bool VnrWriter::writePreamble()
{
    if(!fileHandler)
    {
        return false;
    }

    size_t charsWritten = fwrite(VnrRW::Preamble, sizeof(char), VnrRW::PREAMBLE_LEN, fileHandler);
    if(charsWritten != VnrRW::PREAMBLE_LEN)
    {
        return false;
    }

    return true;
}

bool VnrWriter::writeRecordTime()
{
    if(!firstRecordTime.isValid())
    {
        return false;
    }

    qint64 msecsSinceEpoch = firstRecordTime.toMSecsSinceEpoch();
    char msecsSinceEpochBytes[VnrRW::FILE_WRITE_TIME_SIZE];
    if(!convertValueToHex(msecsSinceEpochBytes, VnrRW::FILE_WRITE_TIME_SIZE, VnrRW::FILE_WRITE_TIME_SIZE, msecsSinceEpoch))
    {
        return false;
    }

    size_t charsWritten = fwrite(msecsSinceEpochBytes, sizeof(char), VnrRW::FILE_WRITE_TIME_SIZE, fileHandler);
    if(charsWritten != VnrRW::FILE_WRITE_TIME_SIZE)
    {
        return false;
    }

    return true;
}

bool VnrWriter::convertValueToHex(char* hexString, size_t stringLen, size_t fieldLen, unsigned long long value)
{
    if((stringLen <= 0) || (stringLen > sizeof(value)))
    {
        return false;
    }

    for(size_t i = 0; i < stringLen; i++)
    {
        hexString[fieldLen - 1 - i] = value >> (i * 8);
    }

    return true;
}

bool VnrWriter::writeDescription()
{
    if(!fileHandler)
    {
        return false;
    }

    const char *descrPtr = description.toLocal8Bit();
    int descriptionSize = strlen(descrPtr);
    const int HEX_STR_LEN = sizeof(int);
    char descSizeData[VnrRW::DESCR_FIELD_SIZE] = {0};

    if(!convertValueToHex(descSizeData, HEX_STR_LEN, HEX_STR_LEN, descriptionSize))
    {
        return false;
    }

    size_t elementsWritten = fwrite(descSizeData, sizeof(char), VnrRW::DESCR_FIELD_SIZE, fileHandler);
    if(elementsWritten != sizeof(int))
    {
        return false;
    }

    if(descriptionSize == 0)
    {
        return true;
    }

    int charsWritten = fwrite(descrPtr, sizeof(char), descriptionSize, fileHandler);
    if(charsWritten != descriptionSize)
    {
        return false;
    }

    return true;
}

void VnrWriter::makeRecordHeader(char *headerBuffer, const VnrRW::RecordTime &recordTime, const VnrRW::RecordData &recordData,
                           const VnrRW::SenderAddress &senderAddress, VnrRW::SenderPort senderPort)
{
    headerBuffer[0] = 0x0D;
    headerBuffer[1] = 0x0A;

    int dataSize = recordData.size();
    headerBuffer[2] = dataSize >> 24;
    headerBuffer[3] = dataSize >> 16;
    headerBuffer[4] = dataSize >> 8;
    headerBuffer[5] = dataSize;

    bool ok;
    quint32 senderAddrBytes = senderAddress.toIPv4Address(&ok);
    if(ok)
    {
        headerBuffer[6] = senderAddrBytes >> 24;
        headerBuffer[7] = senderAddrBytes >> 16;
        headerBuffer[8] = senderAddrBytes >> 8;
        headerBuffer[9] = senderAddrBytes;
    }
    else
    {
        memset(&headerBuffer[6], 0, 4);
    }

    if(senderPort <= 65535)
    {
        headerBuffer[10] = senderPort >> 8;
        headerBuffer[11] = senderPort;
    }
    else
    {
        memset(&headerBuffer[10], 0, 2);
    }

    qint64 msecsOffset = firstRecordTime.msecsTo(recordTime);
    headerBuffer[12] = msecsOffset >> 24;
    headerBuffer[13] = msecsOffset >> 16;
    headerBuffer[14] = msecsOffset >> 8;
    headerBuffer[15] = msecsOffset;
}

bool VnrWriter::write(const QList<std::tuple<VnrRW::RecordTime, VnrRW::RecordData,
                                             VnrRW::SenderAddress, VnrRW::SenderPort>> &data, bool flush)
{
    if(firstRecord)
    {
        if(data.isEmpty())
        {
            return false;
        }

        firstRecordTime = std::get<0>(data[0]);
        if(!writeFileHeader())
        {
            return false;
        }

        firstRecord = false;
    }

    QDateTime recordTime;
    QByteArray recordData;
    QHostAddress senderAddress;
    unsigned int senderPort;

    for(const VnrRW::RecordType &record: data)
    {
        std::tie(recordTime, recordData, senderAddress, senderPort) = record;
        makeRecordHeader(header, recordTime, recordData, senderAddress, senderPort);

        int elementsWritten = fwrite(header, sizeof(char), VnrRW::RECORD_HEADER_LEN, fileHandler);
        if(elementsWritten != VnrRW::RECORD_HEADER_LEN)
        {
            return false;
        }

        elementsWritten = fwrite(recordData.constData(), sizeof(char), recordData.size(), fileHandler);
        if(elementsWritten != recordData.size())
        {
            return false;
        }
    }

    if(flush)
    {
        fflush(fileHandler);
    }


    return true;
}


