#include "DataIOLib.h"
#include <QDebug>
#include <QFileInfo>
#include <QDir>

DataIOLib::DataIOLib()
{

}

QList<QPair<QDateTime, QByteArray>> DataIOLib::readDir(QString dir, int recordTimeshift_ms)
{
    QFileInfo fileInfo(dir);

    if(fileInfo.isFile() && (fileInfo.suffix() == "dat"))
    {
        return readFile(dir,recordTimeshift_ms);
    }

    if(!fileInfo.isDir())
    {
        return QList<QPair<QDateTime, QByteArray>>();
    }

    QList<QPair<QDateTime, QByteArray>> data;
    QDir directory(dir);
    for(QFileInfo &fileInfo: directory.entryInfoList(QStringList() << "*", QDir::AllEntries | QDir::NoDotAndDotDot))
    {
        qDebug() << fileInfo.absoluteFilePath();
        if(fileInfo.isDir())
        {
            data.append(readDir(fileInfo.absoluteFilePath(),recordTimeshift_ms));
        }
        else if(fileInfo.isFile() && (fileInfo.suffix() == "dat"))
        {
            data.append(readFile(fileInfo.absoluteFilePath(),recordTimeshift_ms));
        }
    }

    return data;
}

QList<QPair<QDateTime, QByteArray>> DataIOLib::readFile(QString file, int recordTimeshift_ms, int cutFirstBytes)
{
    QList<QPair<QDateTime, QByteArray>> resultData;

    if(QFileInfo(file).isDir())
    {
        qDebug() << "ERROR! Directory selected instead file: '" << file << "'";
        return resultData;
    }

    static auto BUFFER_SIZE = 0xFFFE;
    static auto TIME_BUFFER_SIZE = 50;
    FILE *fileDesc = fopen(file.toLocal8Bit().data(), "rt");

    if(fileDesc == NULL)
    {
        qDebug() << "ERROR! Can't open '" << file << "'";
        return resultData;
    }

    setlocale(LC_ALL, "C");

    char *stringBuffer = new char[BUFFER_SIZE + 1];
    char *timeStringBuffer = new char[TIME_BUFFER_SIZE + 1];
    bool initialDateTimeFound = false;
    char *dataString = nullptr;
    int mSecs = 0;
    QDateTime initialDateTime;
    while(!feof(fileDesc))
    {
        if(fgets(stringBuffer, BUFFER_SIZE, fileDesc) != nullptr)
        {
            if(stringBuffer[0] == '\0')
            {
                continue;
            }

            if((!initialDateTimeFound) || (stringBuffer[0] == '#'))
            {
                if((stringBuffer[0] == '#') && strchr(stringBuffer, ':') && strchr(stringBuffer, '.'))
                {
                    initialDateTime = getInitialDateTime(stringBuffer);
                    initialDateTimeFound = true;
                }
                continue;
            }

            QDateTime currentTime = initialDateTime;
            mSecs = strtod(stringBuffer, &dataString) * 1000. + recordTimeshift_ms;

            if(dataString[1] == '\n')
            {
                continue;
            }

            int bytesCount = convertStringViewToBin(dataString, stringBuffer);
            resultData << qMakePair(currentTime.addMSecs(mSecs),
                                    QByteArray(stringBuffer + cutFirstBytes, bytesCount - cutFirstBytes));
        }
    }

    delete[] stringBuffer;
    delete[] timeStringBuffer;
    fclose(fileDesc);
    return resultData;
}

AsterixFileInfo::FileDataInfo DataIOLib::getFileDataInfo(QString file, int stringsCount, int cutFirstBytes)
{
    static auto BUFFER_SIZE = 0xFFFE;
    FILE *fileDesc = fopen(file.toLocal8Bit().data(), "rt");

    if(fileDesc == NULL)
    {
        qDebug() << "ERROR! Can't open '" << file << "'";
        return std::make_tuple(QDateTime(), 0, QList<int>());
    }

    setlocale(LC_ALL, "C");

    char *stringBuffer = new char[BUFFER_SIZE + 1];
    bool initialDateTimeFound = false;
    QDateTime initialDateTime;
    QList<int> categories;
    int currentStringNumber = 0;
    while((!feof(fileDesc)) && (currentStringNumber <= stringsCount))
    {
        if(fgets(stringBuffer, BUFFER_SIZE, fileDesc) == nullptr)
        {
            continue;
        }

        if(stringBuffer[0] == '\0')
        {
            continue;
        }

        if((!initialDateTimeFound) || (stringBuffer[0] == '#'))
        {
            if((stringBuffer[0] == '#') && strchr(stringBuffer, ':') && strchr(stringBuffer, '.'))
            {
                initialDateTime = getInitialDateTime(stringBuffer);
                initialDateTimeFound = true;
            }
            continue;
        }

        char *endPtr;
        strtod(stringBuffer, &endPtr);

        int category = strtol(endPtr + (cutFirstBytes * 3), &endPtr, 16);
        if(!categories.contains(category))
        {
            categories << category;
        }
        currentStringNumber++;
    }

    int mSecs = getLastRecordOffetMsecs(fileDesc, stringBuffer, BUFFER_SIZE);

    delete[] stringBuffer;
    fclose(fileDesc);
    return std::make_tuple(initialDateTime, mSecs, categories);
}

int DataIOLib::getLastRecordOffetMsecs(FILE *fileDesc, char *stringBuffer, int bufferSize)
{
    bool pointFound = false;
    double lastTime = 0;
    long endFilePosition = -1;
    if(feof(fileDesc))
    {
        endFilePosition = ftell(fileDesc);
    }
    else
    {
        if(fseek(fileDesc, 0, SEEK_END) == 0)
        {
            endFilePosition = ftell(fileDesc);
        }
    }

    if(endFilePosition > 0)
    {
        long currentFilePosition = endFilePosition;
        while(currentFilePosition >= 0)
        {
            currentFilePosition--;
            if(fseek(fileDesc, currentFilePosition, SEEK_SET) != 0)
            {
                break;
            }

            int readResult = fread(stringBuffer, sizeof(char), 1, fileDesc);
            if(readResult != 1)
            {
                break;
            }

            if(stringBuffer[0] == '.')
            {
                pointFound = true;
            }

            if(!pointFound)
            {
                continue;
            }

            if(!((stringBuffer[0] == '\r') || (stringBuffer[0] == '\n')))
            {
                continue;
            }

            if(fseek(fileDesc, currentFilePosition + 1, SEEK_SET) != 0)
            {
                break;
            }

            if(fgets(stringBuffer, bufferSize, fileDesc) == nullptr)
            {
                break;
            }

            if(strstr(stringBuffer, ".") == nullptr)
            {
                pointFound = false;
                continue;
            }

            sscanf(stringBuffer, "%lf", &lastTime);
            break;
        }
    }

    return lastTime * 1000;
}

/// При использовании функции "strtol" выхода за границу массива не будет,
/// т.к. проход по буферу закончится при достижении символов '\r' или '\n'.
int DataIOLib::convertStringViewToBin(char *inputString, char *outputString)
{
    int digitsCount = 0;
    while((inputString[0] != '\n') && (inputString[0] != '\r'))
    {
        (*outputString) = strtol(inputString, &inputString, 16);
        digitsCount++;
        outputString++;
    }

    return digitsCount;
}

QDateTime DataIOLib::getInitialDateTime(char *dateTimeString)
{
    QString dateTimeQString(dateTimeString);
    dateTimeQString = dateTimeQString.remove("#").trimmed();
    return QDateTime::fromString(dateTimeQString, "dd.MM.yyyy hh:mm:ss");
}




