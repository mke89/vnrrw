#include "PcapReader.h"
#include <stdio.h>
#include <QByteArray>

typedef struct
{
    unsigned int   magic_number;    // magic number
    unsigned short version_major;   // major version number
    unsigned short version_minor;   // minor version number
    signed   int   thiszone;        // GMT to local correction
    unsigned int   sigfigs;         // accuracy of timestamps
    unsigned int   snaplen;         // max length of captured packets, in octets
    unsigned int   network;         // data link type
} PcapFileHeader_t;

typedef struct
{
    unsigned int ts_sec;            // timestamp seconds
    unsigned int ts_usec;           // timestamp microseconds
    unsigned int incl_len;          // number of octets of packet saved in file
    unsigned int orig_len;          // actual length of packet
} PcapRecordHeader_t;

typedef enum
{
    ICMP    = 2,
    TCP     = 6,
    UDP     = 17,

} ProtocolType_t;

typedef enum
{
    RECORD_HEADER_SIZE  = sizeof(PcapRecordHeader_t),

} PcapParams_t;

PcapReader::PcapReader():
    pcapFileHandler(nullptr),
    pcapServiceData{false, NETWORK_TYPE_UNKNOWN},
    fileName(QString()),
    startDateTime(QDateTime()),
    pcapSettings(PcapSettings_t())
{

}

PcapReader::PcapReader(QString file, QDateTime startDateTime):
    pcapFileHandler(nullptr),
    pcapServiceData{false, NETWORK_TYPE_UNKNOWN},
    fileName(file),
    startDateTime(startDateTime),
    pcapSettings(PcapSettings_t())
{
    open();
}

void PcapReader::close()
{
    clearResources(&pcapFileHandler, &startDateTime);
}

void PcapReader::clearResources(FILE **fileHandler, QDateTime *startDateTime)
{
    if((!(*fileHandler)) || (!startDateTime))
    {
        return;
    }

    fclose((*fileHandler));
    (*fileHandler) = nullptr;
    (*startDateTime) = QDateTime();
}

PcapReader::~PcapReader()
{
    close();
}

void PcapReader::setFile(QString file)
{
    fileName = file;
}

bool PcapReader::fileOpened()
{
    return pcapFileHandler;
}

bool PcapReader::open()
{
    return openFile(fileName, &pcapFileHandler, &pcapServiceData, &startDateTime);
}

bool PcapReader::openFile(QString fileName, FILE **fileHandler, PcapServiceData_t *serviceData, QDateTime *startDateTime)
{
    if((!fileName.endsWith(".pcap")) || fileName.isEmpty())
    {
        return false;
    }

    if((!serviceData) || (!startDateTime))
    {
        return false;
    }

    (*fileHandler) = fopen(fileName.toLocal8Bit().data(), "rb");
    if(!(*fileHandler))
    {
        return false;
    }

    PcapFileHeader_t pcapFileHeader;
    if(fread(&pcapFileHeader, sizeof(PcapFileHeader_t), 1, *fileHandler) != 1)
    {
        clearResources(fileHandler, startDateTime);
        return false;
    }

    // Find input file format
    if(pcapFileHeader.magic_number == 0xA1B2C3D4)
    {
        serviceData->pcapInverted = false;
    }
    else if(pcapFileHeader.magic_number == 0xD4C3B2A1)
    {
        serviceData->pcapInverted = true;
    }
    else
    {
        clearResources(fileHandler, startDateTime);
        return false;
    }

    if(serviceData->pcapInverted)
    {
        pcapFileHeader.network = ConvertInt(pcapFileHeader.network);
    }

    if(pcapFileHeader.network == 1)
    {
        serviceData->pcapNetworkType = NETWORK_TYPE_ETHERNET;
    }
    else if(pcapFileHeader.network == 113)
    {
        serviceData->pcapNetworkType = NETWORK_TYPE_LINUX;
    }
    else
    {
        clearResources(fileHandler, startDateTime);
        return false;
    }

    (*startDateTime) = QDateTime();

    return true;
}

QList<VnrRW::RecordType> PcapReader::readRecord(FILE *fileHandler, const PcapSettings_t &userSettings,
                                                                   const PcapServiceData_t &internalSettings)
{
    QList<VnrRW::RecordType> records;

    if(!fileHandler)
    {
        return records;
    }

    PcapRecordHeader_t pcapRecordHeader;
    if(fread(&pcapRecordHeader, sizeof(PcapRecordHeader_t), 1, fileHandler) != 1)
    {
        return records;
    }

    unsigned int dataLength = 0;
    if(internalSettings.pcapInverted)
    {
        dataLength = ConvertInt(pcapRecordHeader.incl_len);
    }
    else
    {
        dataLength = pcapRecordHeader.incl_len;
    }

    char dataBuffer[dataLength];

    if(fread(dataBuffer, sizeof(char), dataLength, fileHandler) != dataLength)
    {
        return records;
    }

    unsigned char *packetData = (unsigned char *)dataBuffer;
    if(internalSettings.pcapNetworkType == NETWORK_TYPE_ETHERNET)
    {
        packetData += 6; // Destination MAC
        packetData += 6; // Source MAC
    }
    else if(internalSettings.pcapNetworkType == NETWORK_TYPE_LINUX)
    {
        packetData += 2; // Packet type
        packetData += 2; // link-layer Address type
        packetData += 2; // Address length
        packetData += 8; // Source
    }

    bool ipInverted = false;
    {
        unsigned short protocol = *((unsigned short*)packetData);
        packetData += 2;
        if(internalSettings.pcapInverted)
        {
            protocol = ConvertShort(protocol);
        }

        // Check if byte order should be reverted
        if(protocol == 0x0008)
        {
            ipInverted = !internalSettings.pcapInverted;
        }
        else if(protocol == 0x0800)
        {
            ipInverted =  internalSettings.pcapInverted;
        }
        else
        {
            return records;
        }
    }

    // parse IP header
    unsigned char ipHeaderLength = *packetData;
    packetData++;
    ipHeaderLength &= 0x0F;

    packetData += 1;    // TOS

    unsigned short ipTotalLength = *((unsigned short*)packetData); // Total length
    packetData += 2;
    if(ipInverted)
    {
        ipTotalLength = ConvertShort(ipTotalLength);
    }

    packetData += 5;    // ID + Flags + Offset + TTL

    unsigned char protocol = *packetData;
    packetData++;

    if(protocol != UDP)
    {
        return records;
    }
    int sourceAddrB1 = 0;
    int sourceAddrB2 = 0;
    int sourceAddrB3 = 0;
    int sourceAddrB4 = 0;

    // UDP
    packetData += 2;    // Checksum
    sourceAddrB1 = *(packetData + 0);
    sourceAddrB2 = *(packetData + 1);
    sourceAddrB3 = *(packetData + 2);
    sourceAddrB4 = *(packetData + 3);
    packetData += 4;    // Source IP

    uint32_t ipDestAddr = 0;
    ipDestAddr = *((uint32_t*)packetData);
    packetData += 4; // destination IP

    if(ipInverted)
    {
        ipDestAddr = ConvertInt(ipDestAddr);
    }

    packetData += ipHeaderLength * 4 - 20; // options
    ipTotalLength -= ipHeaderLength * 4;


    // UDP header
    unsigned short udpSrcPort = *((unsigned short*)packetData);
    packetData += 2;    // source port
    if(ipInverted)
    {
        udpSrcPort = ConvertShort(udpSrcPort);
    }

    unsigned short udpDestPort = *((unsigned short*)packetData);
    packetData += 2;    // destination port
    if(ipInverted)
    {
        udpDestPort = ConvertShort(udpDestPort);
    }

    unsigned short udpDataLength = *((unsigned short*)packetData);
    packetData += 2;    // data length
    if(ipInverted)
    {
        udpDataLength = ConvertShort(udpDataLength);
    }

    if(ipTotalLength != udpDataLength)
    {
        return records;
    }

    packetData += 2;    // checksum
    udpDataLength -= 8;

    if((packetData[0] == userSettings.startFirstMarker) &&
       (packetData[1] == userSettings.startSecondMarker) &&
       (packetData[udpDataLength - 2] == userSettings.endFirstMarker) &&
       (packetData[udpDataLength - 1] == userSettings.endSecondMarker))
    {
        packetData += 2;    // checksum
        udpDataLength -= 4;
    }

    QString ipAddress = QString("%1.%2.%3.%4").arg(QString::number(sourceAddrB1)).
                                               arg(QString::number(sourceAddrB2)).
                                               arg(QString::number(sourceAddrB3)).
                                               arg(QString::number(sourceAddrB4));
    QHostAddress srcIp = QHostAddress(ipAddress);
    unsigned int srcPort = udpSrcPort;
    QDateTime recordTime = QDateTime::fromSecsSinceEpoch(pcapRecordHeader.ts_sec, Qt::LocalTime).addMSecs(pcapRecordHeader.ts_usec / 1000);

    bool multiRecords = false;
    unsigned short currentByte = 0;
    while(currentByte < udpDataLength)
    {
        if(packetData[currentByte] != userSettings.endFirstMarker)
        {
            currentByte++;
            continue;
        }

        if(packetData[currentByte + 1] != userSettings.endSecondMarker)
        {
            currentByte++;
            continue;
        }

        if(packetData[currentByte + 2] != userSettings.startFirstMarker)
        {
            currentByte++;
            continue;
        }

        if(packetData[currentByte + 3] != userSettings.startSecondMarker)
        {
            currentByte++;
            continue;
        }

        multiRecords = true;
        records << std::make_tuple(recordTime, QByteArray((const char*)packetData, currentByte), srcIp, srcPort);

        udpDataLength -= (currentByte + 4);
        packetData += (currentByte + 4);
        currentByte = 0;
    }

    if(multiRecords)
    {
        records << std::make_tuple(recordTime, QByteArray((const char*)packetData, currentByte), srcIp, srcPort);
    }
    else
    {
        records << std::make_tuple(recordTime, QByteArray((const char*)packetData, udpDataLength), srcIp, srcPort);
    }

    return records;
}

AsterixFileInfo::FileDataInfo PcapReader::getFileDataInfo(QString fileName, int recordsCount, PcapSettings_t settings)
{
    FILE *fileHandler = nullptr;
    PcapServiceData_t serviceData;
    QDateTime startDateTime;
    if(!openFile(fileName, &fileHandler, &serviceData, &startDateTime))
    {
        clearResources(&fileHandler, &startDateTime);
        return AsterixFileInfo::FileDataInfo();
    }

    QList<int> categoriesList;
    QDateTime firstRecordTime;
    QDateTime lastRecordTime;
    int currentRecordNumber = 0;
    bool stopFlag = false;
    while((!feof(fileHandler)) && (currentRecordNumber <= recordsCount))
    {
        const QList<VnrRW::RecordType> &records = readRecord(fileHandler, settings, serviceData);
        if(records.isEmpty())
        {
            stopFlag = true;
            break;
        }

        for(const VnrRW::RecordType &record: records)
        {
            const QDateTime &recordTime = std::get<0>(record);
            const QByteArray &recordData = std::get<1>(record);
            if(recordData.isEmpty() || (!recordTime.isValid()))
            {
                stopFlag = true;
            }

            if(!firstRecordTime.isValid())
            {
                firstRecordTime = recordTime;
            }
            lastRecordTime = recordTime;

            int recordCategory = (unsigned char)recordData[0];
            if(!categoriesList.contains(recordCategory))
            {
                categoriesList << recordCategory;
            }
            currentRecordNumber++;
        }

        if(stopFlag)
        {
            break;
        }
    }

    if((!firstRecordTime.isValid()) || categoriesList.isEmpty() || (!lastRecordTime.isValid()))
    {
        return AsterixFileInfo::FileDataInfo();
    }

    if(stopFlag)
    {
        clearResources(&fileHandler, &startDateTime);
        return std::make_tuple(firstRecordTime, firstRecordTime.msecsTo(lastRecordTime), categoriesList);
    }

    QDateTime lastTime = getLastRecordTime(&fileHandler, serviceData.pcapInverted);
    if(!lastTime.isValid())
    {
        return AsterixFileInfo::FileDataInfo();
    }

    clearResources(&fileHandler, &startDateTime);
    return std::make_tuple(firstRecordTime, firstRecordTime.msecsTo(lastTime), categoriesList);
}

AsterixFileInfo::RecordTime PcapReader::getLastRecordTime(FILE **fileHandler, bool pcapInverted)
{
    long endFilePosition = -1;
    QDateTime lastTime;
    if(feof(*fileHandler))
    {
        endFilePosition = ftell(*fileHandler);
    }
    else
    {
        if(fseek(*fileHandler, 0, SEEK_END) == 0)
        {
            endFilePosition = ftell(*fileHandler);
        }
    }

    if(endFilePosition <= 0)
    {
        return lastTime;
    }

    if(fseek(*fileHandler, endFilePosition - RECORD_HEADER_SIZE, SEEK_SET) != 0)
    {
        return lastTime;
    }

    long currentFilePosition = ftell(*fileHandler);
    unsigned int offsetBytes = 0;
    char headerData[RECORD_HEADER_SIZE];
    PcapRecordHeader_t* recordHeader = (PcapRecordHeader_t*)headerData;
    while(currentFilePosition >= 0)
    {
        int readResult = fread(headerData, sizeof(char), RECORD_HEADER_SIZE, *fileHandler);
        if(readResult < RECORD_HEADER_SIZE)
        {
            return lastTime;
        }

        if(recordHeader->incl_len != recordHeader->orig_len)
        {
            offsetBytes++;
            currentFilePosition--;
            if(fseek(*fileHandler, currentFilePosition, SEEK_SET) != 0)
            {
                return lastTime;
            }
            continue;
        }

        unsigned int dataLength = 0;
        if(pcapInverted)
        {
            dataLength = ConvertInt(recordHeader->incl_len);
        }
        else
        {
            dataLength = recordHeader->incl_len;
        }

        if(offsetBytes != dataLength)
        {
            offsetBytes++;
            currentFilePosition--;
            if(fseek(*fileHandler, currentFilePosition, SEEK_SET) != 0)
            {
                return lastTime;
            }
            continue;
        }

        lastTime = QDateTime::fromSecsSinceEpoch(recordHeader->ts_sec, Qt::LocalTime).addMSecs(recordHeader->ts_usec / 1000);
        break;
    }

    return lastTime;
}

QList<VnrRW::RecordType> PcapReader::readAll()
{
    QList<VnrRW::RecordType> records;
    if(!pcapFileHandler)
    {
        return records;
    }

    while(!feof(pcapFileHandler))
    {
        const QList<VnrRW::RecordType> &currentRecords = readRecord(pcapFileHandler, pcapSettings, pcapServiceData);
        if(currentRecords.isEmpty())
        {
            continue;
        }
        records << currentRecords;
    }

    close();
    return records;
}

QList<QPair<QDateTime, QByteArray>> PcapReader::readFile(QString file, PcapSettings_t pcapSettings)
{
    QList<QPair<QDateTime, QByteArray>> records;
    FILE *fileHandler = nullptr;
    QDateTime startDateTime;
    PcapServiceData_t serviceData;

    if(!openFile(file, &fileHandler, &serviceData, &startDateTime))
    {
        clearResources(&fileHandler, &startDateTime);
        return records;
    }

    if(!fileHandler)
    {
        clearResources(&fileHandler, &startDateTime);
        return records;
    }

    while(true)
    {
        const QList<VnrRW::RecordType> &currentRecords = readRecord(fileHandler, pcapSettings, serviceData);
        if(currentRecords.isEmpty())
        {
            break;
        }
        for(const VnrRW::RecordType &currentRecord: currentRecords)
        {
            records << qMakePair(std::get<0>(currentRecord), std::get<1>(currentRecord));
        }
    }

    clearResources(&fileHandler, &startDateTime);
    return records;
}

uint32_t PcapReader::ConvertInt(uint32_t in)
{
    uint32_t out;
    char *p_in  = (char *) &in;
    char *p_out = (char *) &out;

    p_out[0] = p_in[3];
    p_out[1] = p_in[2];
    p_out[2] = p_in[1];
    p_out[3] = p_in[0];

    return out;
}

uint16_t PcapReader::ConvertShort(uint16_t in)
{
    uint16_t out;
    char *p_in  = (char *) &in;
    char *p_out = (char *) &out;

    p_out[0] = p_in[1];
    p_out[1] = p_in[0];

    return out;
}

void PcapReader::setStartMarkers(char first, char second)
{
    pcapSettings.startFirstMarker = first;
    pcapSettings.startSecondMarker = second;
}

void PcapReader::setEndMarkers(char first, char second)
{
    pcapSettings.endFirstMarker = first;
    pcapSettings.endSecondMarker = second;
}

QPair<char, char> PcapReader::getStartMarkers()
{
    return qMakePair(pcapSettings.startFirstMarker, pcapSettings.startSecondMarker);
}

QPair<char, char> PcapReader::getEndMarkers()
{
    return qMakePair(pcapSettings.endFirstMarker, pcapSettings.endSecondMarker);
}




