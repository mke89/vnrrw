#ifndef ASTERIXREADERFACADE_H
#define ASTERIXREADERFACADE_H

#include <QString>

#include "VnrRWTypes.h"
#include "VnrReader.h"
#include "PcapReader.h"
#include "DataIOLib.h"

namespace AsterixFileType
{
    enum FileType
    {
        UNDEFINED,
        DAT,
        VNR,
        PCAP
    };
}

class CommonAsterixReader
{
    public:
        CommonAsterixReader(QString fileName);
        CommonAsterixReader(QString fileName, int timeShift_ms);
        ~CommonAsterixReader();

        void setFile(QString fileName);
        bool fileOpened();
        bool open();
        void close();
        QList<VnrRW::RecordType> readAll(int cutFirstBytes = 0);
        static AsterixFileInfo::FileDataInfo getFileDataInfo(QString fileName, int recordsCount, int cutFirstBytes = 0);
        static QList<QPair<QDateTime, QByteArray>> readDir(QString dir, int timeShift_ms = 0);
        static QList<QPair<QDateTime, QByteArray>> readFile(QString file, int timeShift_ms = 0);

        // For "VnrReader"
        QString getDescription();
        quint32 getDescriptionSize();
        QDateTime getInitialTime();

        // For "PcapReader"
        void setStartMarkers(char first, char second);
        void setEndMarkers(char first, char second);
        QPair<char, char> getStartMarkers();
        QPair<char, char> getEndMarkers();

    private:
        QString fileName;
        VnrReader vnrReader;
        PcapReader pcapReader;
        AsterixFileType::FileType fileType;
        int recordTimeshift;

        AsterixFileType::FileType defineFileType(QString fileName);
};

#endif // ASTERIXREADERFACADE_H
