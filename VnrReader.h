#ifndef VNRREADER_H
#define VNRREADER_H

#include <QByteArray>
#include <QString>
#include <QDateTime>
#include "VnrRWTypes.h"

struct DataIndex
{
    QString key;
    int position;
    bool valid;

    DataIndex():
        key(QString()),
        position(-1),
        valid(false)
    {}
};

class VnrReader
{
    public:
        VnrReader();
        VnrReader(QString file);
        ~VnrReader();

        void setFile(QString file);
        bool fileOpened();
        bool open();
        void close();
        QList<VnrRW::RecordType> readAll();
        static AsterixFileInfo::FileDataInfo getFileDataInfo(QString fileName, int stringsCount = 100);
        static QList<QPair<QDateTime, QByteArray>> readFile(QString file);
        
        QString getDescription();
        quint32 getDescriptionSize();
        QDateTime getInitialTime();

    private:
        struct VnrReaderParams
        {
            QString fileName;
            FILE **fileHandler;
            QDateTime *startDateTime;
            char *preamble;
            char *writeFileTimeBuf;
            QString *description;

            VnrReaderParams():
                fileName(QString()),
                fileHandler(nullptr),
                startDateTime(nullptr),
                preamble(nullptr),
                writeFileTimeBuf(nullptr),
                description(nullptr)
            {}

            VnrReaderParams(QString fileName, FILE **fileHandler, QDateTime *startDateTime,
                            char *preamble, char *writeFileTimeBuf, QString *description):
                fileName(fileName),
                fileHandler(fileHandler),
                startDateTime(startDateTime),
                preamble(preamble),
                writeFileTimeBuf(writeFileTimeBuf),
                description(description)
            {}
        };

        struct VnrRecordHeader
        {
            uchar packetsDelimeter[VnrRW::DELIMETER_LEN];
            uint dataSize;
            uchar senderIp[VnrRW::SENDER_ADDR_LEN];
            ushort senderPort;
            uint recordTimeOffsetMs;

            VnrRecordHeader():
                packetsDelimeter{0, 0},
                dataSize{0},
                senderIp{0, 0, 0, 0},
                senderPort{0},
                recordTimeOffsetMs{0}
            {}
        };

        QString fileName;
        FILE *fileHandler;
        quint32 descriptionSize;
        QString description;
        VnrRW::RecordTime initialTime;

        char preamble[VnrRW::PREAMBLE_LEN + 1];
        char writeFileTimeBuf[VnrRW::FILE_WRITE_TIME_SIZE];

        static VnrRW::RecordType readRecord(FILE *fileHandler, char *headerBuf,
                                            const VnrRW::RecordTime &initialTime);
        static VnrRecordHeader readRecordHeader(FILE **fileHandler, char *headerBuf);
        static bool readPreamble(FILE *fileHandler, char *buffer);
        static bool readFileWritetime(FILE *fileHandler, char *buffer);
        static qint64 parseFileWritetime(char *buffer);
        static QPair<QString, quint32> readDescription(FILE *fileHandler);

        static bool openFile(VnrReaderParams &vnrReaderParams);
        static void clearResources(FILE **fileHandler, quint32 &descriptionSize,
                                   QString &description, VnrRW::RecordTime &initialTime);
        static AsterixFileInfo::RecordTime getLastRecordTime(FILE **fileHandler, const VnrRW::RecordTime &initialTime);
};

#endif // VNRREADER_H
