#include "VnrReader.h"

VnrReader::VnrReader(): fileHandler(nullptr)
{
}

VnrReader::VnrReader(QString file):
    fileName(file),
    fileHandler(nullptr),
    descriptionSize(0)
{
    open();
}

VnrReader::~VnrReader()
{
    close();
}

void VnrReader::setFile(QString file)
{
    fileName = file;
}

bool VnrReader::fileOpened()
{
    return fileHandler;
}

bool VnrReader::open()
{
    VnrReaderParams vnrReaderParams = {fileName, &fileHandler, &initialTime,
                                       preamble, writeFileTimeBuf, &description};
    if(!openFile(vnrReaderParams))
    {
        clearResources(&fileHandler, descriptionSize, description, initialTime);
        return false;
    }

    return true;
}

bool VnrReader::openFile(VnrReaderParams &vnrReaderParams)
{
    if((!vnrReaderParams.fileName.endsWith(".vnr")) || (vnrReaderParams.fileName.isEmpty()))
    {
        return false;
    }

    if((!vnrReaderParams.fileHandler) || (!vnrReaderParams.startDateTime) ||
       (!vnrReaderParams.preamble) || (!vnrReaderParams.writeFileTimeBuf) ||
       (!vnrReaderParams.description))
    {
        return false;
    }

    *vnrReaderParams.fileHandler = fopen(vnrReaderParams.fileName.toStdString().c_str(), "rb");
    if(!vnrReaderParams.fileHandler)
    {
        return false;
    }

    quint32 descriptionSize = 0;
    VnrRW::RecordTime initialTime;

    if(!readPreamble(*vnrReaderParams.fileHandler, vnrReaderParams.preamble))
    {
        clearResources(vnrReaderParams.fileHandler, descriptionSize,
                       *vnrReaderParams.description, initialTime);
        return false;
    }

    if(!readFileWritetime(*vnrReaderParams.fileHandler, vnrReaderParams.writeFileTimeBuf))
    {
        clearResources(vnrReaderParams.fileHandler, descriptionSize,
                       *vnrReaderParams.description, initialTime);
        return false;
    }

    qint64 msecsSinceEpoch = parseFileWritetime(vnrReaderParams.writeFileTimeBuf);
    (*vnrReaderParams.startDateTime) = QDateTime::fromMSecsSinceEpoch(msecsSinceEpoch);

    QPair<QString, quint32> descriptionInfo = readDescription(*vnrReaderParams.fileHandler);
    *vnrReaderParams.description = descriptionInfo.first;

    return true;
}

void VnrReader::clearResources(FILE **fileHandler, quint32 &descriptionSize,
                               QString &description, VnrRW::RecordTime &initialTime)
{
    if(!(*fileHandler))
    {
        return;
    }

    fclose(*fileHandler);
    *fileHandler = nullptr;
    descriptionSize = 0;
    description.clear();
    initialTime = QDateTime();
}

void VnrReader::close()
{
    clearResources(&fileHandler, descriptionSize, description, initialTime);
}

QString VnrReader::getDescription()
{
    return description;
}

quint32 VnrReader::getDescriptionSize()
{
    return descriptionSize;
}

QDateTime VnrReader::getInitialTime()
{
    return initialTime;
}

bool VnrReader::readPreamble(FILE *fileHandler, char *buffer)
{
    if(!fileHandler)
    {
        return false;
    }

    if(fread(buffer, sizeof(char), VnrRW::PREAMBLE_LEN, fileHandler) != VnrRW::PREAMBLE_LEN)
    {
        return false;
    }

    buffer[VnrRW::PREAMBLE_LEN] = '\0';

    return true;
}

bool VnrReader::readFileWritetime(FILE *fileHandler, char *buffer)
{
    if(!fileHandler)
    {
        return false;
    }

    if(fread(buffer, sizeof(char), VnrRW::FILE_WRITE_TIME_SIZE, fileHandler) != VnrRW::FILE_WRITE_TIME_SIZE)
    {
        return false;
    }

    return true;
}

qint64 VnrReader::parseFileWritetime(char *buffer)
{
    qint64 fileWriteTimeMsFromUtc = 0;
    for(int i = 0; i < VnrRW::FILE_WRITE_TIME_SIZE; i++)
    {
        fileWriteTimeMsFromUtc = (fileWriteTimeMsFromUtc << 8) | (unsigned char)buffer[i];
    }

    return fileWriteTimeMsFromUtc;
}

QPair<QString, quint32> VnrReader::readDescription(FILE *fileHandler)
{
    if(!fileHandler)
    {
        return qMakePair(QString(), 0);
    }

    char descriptionSizeBuf[VnrRW::DESCR_FIELD_SIZE];
    if(fread(descriptionSizeBuf, sizeof(char), VnrRW::DESCR_FIELD_SIZE, fileHandler) != VnrRW::DESCR_FIELD_SIZE)
    {
        return qMakePair(QString(), 0);
    }

    quint32 descriptionSize = 0;
    for(int i = 0; i < VnrRW::DESCR_FIELD_SIZE; i++)
    {
        descriptionSize = (descriptionSize << 8) | descriptionSizeBuf[i];
    }

    if(descriptionSize == 0)
    {
        return qMakePair(QString(), 0);
    }

    char descriptionBuf[descriptionSize + 1];
    if(fread(descriptionBuf, sizeof(char), descriptionSize, fileHandler) != descriptionSize)
    {
        return qMakePair(QString(), 0);
    }

    descriptionBuf[descriptionSize] = '\0';
    return qMakePair(QString(descriptionBuf), descriptionSize);
}

QList<VnrRW::RecordType> VnrReader::readAll()
{
    QList<std::tuple<VnrRW::RecordTime, VnrRW::RecordData,
                     VnrRW::SenderAddress, VnrRW::SenderPort>> records;

    if(!initialTime.isValid())
    {
        return records;
    }

    char headerBuf[VnrRW::RECORD_HEADER_LEN];
    while(true)
    {
        const VnrRW::RecordType &record = readRecord(fileHandler, headerBuf, initialTime);
        if(!std::get<0>(record).isValid())
        {
            break;
        }
        records << record;
    }

    close();
    return records;
}

QList<QPair<QDateTime, QByteArray>> VnrReader::readFile(QString file)
{
    QList<QPair<QDateTime, QByteArray>> records;

    VnrRW::RecordTime initialTime;
    char preamble[VnrRW::PREAMBLE_LEN + 1];
    char writeFileTimeBuf[VnrRW::FILE_WRITE_TIME_SIZE];
    QString description;
    FILE *fileHandler;
    quint32 descriptionSize;

    VnrReaderParams vnrReaderParams = {file, &fileHandler, &initialTime,
                                       preamble, writeFileTimeBuf, &description};
    if(!openFile(vnrReaderParams))
    {
        clearResources(&fileHandler, descriptionSize, description, initialTime);
        return records;
    }

    if(!initialTime.isValid())
    {
        return records;
    }

    char headerBuf[VnrRW::RECORD_HEADER_LEN];
    while(true)
    {
        const VnrRW::RecordType &record = readRecord(fileHandler, headerBuf, initialTime);
        if(!std::get<0>(record).isValid())
        {
            break;
        }
        records << qMakePair(std::get<0>(record), std::get<1>(record));
    }


    return records;
}

AsterixFileInfo::FileDataInfo VnrReader::getFileDataInfo(QString fileName, int recordsCount)
{
    FILE *fileHandler = nullptr;
    QDateTime initialTime;
    QString description;
    quint32 descriptionSize;
    char preamble[VnrRW::PREAMBLE_LEN + 1];
    char writeFileTimeBuf[VnrRW::FILE_WRITE_TIME_SIZE];

    VnrReaderParams vnrReaderParams = {fileName, &fileHandler, &initialTime,
                                       preamble, writeFileTimeBuf, &description};
    if(!openFile(vnrReaderParams))
    {
        clearResources(&fileHandler, descriptionSize, description, initialTime);
        return AsterixFileInfo::FileDataInfo();
    }

    QList<int> categoriesList;
    QDateTime lastRecordTime;
    int currentRecordNumber = 0;

    char headerBuf[VnrRW::RECORD_HEADER_LEN];
    while((!feof(fileHandler)) && (currentRecordNumber <= recordsCount))
    {
        const VnrRW::RecordType &record = readRecord(fileHandler, headerBuf, initialTime);
        const QDateTime &recordTime = std::get<0>(record);
        const QByteArray &recordData = std::get<1>(record);
        if((!recordTime.isValid()) || recordData.isEmpty())
        {
            break;
        }

        lastRecordTime = recordTime;

        int recordCategory = (unsigned char)recordData[0];
        if(!categoriesList.contains(recordCategory))
        {
            categoriesList << recordCategory;
        }
        currentRecordNumber++;
    }

    if(categoriesList.isEmpty() || (!lastRecordTime.isValid()))
    {
        return AsterixFileInfo::FileDataInfo();
    }

    if(currentRecordNumber <= recordsCount)
    {
        clearResources(&fileHandler, descriptionSize, description, initialTime);
        return std::make_tuple(initialTime, initialTime.msecsTo(lastRecordTime), categoriesList);
    }

    QDateTime lastTime = getLastRecordTime(&fileHandler, initialTime);
    if(!lastTime.isValid())
    {
        return AsterixFileInfo::FileDataInfo();
    }

    const AsterixFileInfo::FileDataInfo &fileInfo =
            std::make_tuple(initialTime, initialTime.msecsTo(lastTime), categoriesList);
    clearResources(&fileHandler, descriptionSize, description, initialTime);
    return fileInfo;
}

AsterixFileInfo::RecordTime VnrReader::getLastRecordTime(FILE **fileHandler, const VnrRW::RecordTime &initialTime)
{
    long endFilePosition = -1;
    QDateTime lastTime;
    if(feof(*fileHandler))
    {
        endFilePosition = ftell(*fileHandler);
    }
    else
    {
        if(fseek(*fileHandler, 0, SEEK_END) == 0)
        {
            endFilePosition = ftell(*fileHandler);
        }
    }

    if(endFilePosition <= 0)
    {
        return lastTime;
    }

    if(fseek(*fileHandler, endFilePosition - VnrRW::RECORD_HEADER_LEN, SEEK_SET) != 0)
    {
        return lastTime;
    }

    long currentFilePosition = ftell(*fileHandler);
    int offsetBytes = 0;
    char headerData[VnrRW::RECORD_HEADER_LEN];
    while(currentFilePosition >= 0)
    {
        const VnrRecordHeader &recordHeader = readRecordHeader(fileHandler, headerData);
        if((recordHeader.packetsDelimeter[0] != 0x0D) && (recordHeader.packetsDelimeter[1] != 0x0A))
        {
            offsetBytes++;
            currentFilePosition--;
            if(fseek(*fileHandler, currentFilePosition, SEEK_SET) != 0)
            {
                return lastTime;
            }
            continue;
        }

        if(((recordHeader.packetsDelimeter[0] == 0x0D) && (recordHeader.packetsDelimeter[1] == 0x0A)) &&
           (recordHeader.dataSize != offsetBytes))
        {
            offsetBytes = -(VnrRW::RECORD_HEADER_LEN - 1);
            currentFilePosition--;
            if(fseek(*fileHandler, currentFilePosition, SEEK_SET) != 0)
            {
                return lastTime;
            }
            continue;
        }

        lastTime = initialTime.addMSecs(recordHeader.recordTimeOffsetMs);
        break;
    }

    return lastTime;
}

VnrRW::RecordType VnrReader::readRecord(FILE *fileHandler, char *headerBuf,
                                        const VnrRW::RecordTime &initialTime)
{
    VnrRW::RecordType record;

    if((!fileHandler) || (!headerBuf) || (!initialTime.isValid()))
    {
        return record;
    }

    if(fread(headerBuf, sizeof(char), VnrRW::RECORD_HEADER_LEN, fileHandler) != VnrRW::RECORD_HEADER_LEN)
    {
        return record;
    }

    if((headerBuf[0] != 0x0D) || (headerBuf[1] != 0x0A))
    {
        return record;
    }

    int dataLength = 0;
    for(int i = 0; i < VnrRW::DATA_LENGTH_LEN; i++)
    {
        dataLength = (dataLength << 8) | (unsigned char)headerBuf[i + VnrRW::DELIMETER_LEN];
    }

    int ipAddrOffset = VnrRW::IP_ADDR_OFFSET;
    QString ipAddress = QString("%1.%2.%3.%4").arg(QString::number(headerBuf[ipAddrOffset])).
                                               arg(QString::number(headerBuf[ipAddrOffset + 1])).
                                               arg(QString::number(headerBuf[ipAddrOffset + 2])).
                                               arg(QString::number(headerBuf[ipAddrOffset + 3]));

    QHostAddress senderAddress(ipAddress);

    int protOffset = VnrRW::PORT_OFFSET;
    unsigned int senderPort = 0;
    senderPort = (unsigned char)headerBuf[protOffset];
    senderPort = (senderPort << 8) | (unsigned char)headerBuf[protOffset + 1];

    quint32 recordTimeOffsetMs = 0;
    for(int i = 0; i < VnrRW::SEND_TIME_LEN; i++)
    {
        recordTimeOffsetMs = (recordTimeOffsetMs << 8) |
                             (unsigned char)headerBuf[VnrRW::RECORD_TIME_OFFSET + i];
    }

    char data[dataLength];
    if(fread(data, sizeof(char), dataLength, fileHandler) != (size_t)dataLength)
    {
        return record;
    }

    record = std::make_tuple(QDateTime(initialTime).addMSecs(recordTimeOffsetMs),
                             QByteArray(data, dataLength), senderAddress, senderPort);
    return record;
}

VnrReader::VnrRecordHeader VnrReader::readRecordHeader(FILE **fileHandler, char *headerBuf)
{
    VnrRecordHeader recordHeader;

    if((!(*fileHandler)) || (!headerBuf))
    {
        return recordHeader;
    }

    if(fread(headerBuf, sizeof(char), VnrRW::RECORD_HEADER_LEN, *fileHandler) != VnrRW::RECORD_HEADER_LEN)
    {
        return recordHeader;
    }

    if((headerBuf[0] != 0x0D) || (headerBuf[1] != 0x0A))
    {
        return recordHeader;
    }

    recordHeader.packetsDelimeter[0] = 0x0D;
    recordHeader.packetsDelimeter[1] = 0x0A;

    int dataLength = 0;
    for(int i = 0; i < VnrRW::DATA_LENGTH_LEN; i++)
    {
        dataLength = (dataLength << 8) | (unsigned char)headerBuf[i + VnrRW::DELIMETER_LEN];
    }
    recordHeader.dataSize = dataLength;

    int ipAddrOffset = VnrRW::IP_ADDR_OFFSET;
    recordHeader.senderIp[0] = headerBuf[ipAddrOffset];
    recordHeader.senderIp[1] = headerBuf[ipAddrOffset + 1];
    recordHeader.senderIp[2] = headerBuf[ipAddrOffset + 2];
    recordHeader.senderIp[3] = headerBuf[ipAddrOffset + 3];

    QString ipAddress = QString("%1.%2.%3.%4").arg(QString::number(headerBuf[ipAddrOffset])).
                                               arg(QString::number(headerBuf[ipAddrOffset + 1])).
                                               arg(QString::number(headerBuf[ipAddrOffset + 2])).
                                               arg(QString::number(headerBuf[ipAddrOffset + 3]));

    QHostAddress senderAddress(ipAddress);

    int protOffset = VnrRW::PORT_OFFSET;
    unsigned int senderPort = 0;
    senderPort = (unsigned char)headerBuf[protOffset];
    senderPort = (senderPort << 8) | (unsigned char)headerBuf[protOffset + 1];
    recordHeader.senderPort = senderPort;

    quint32 recordTimeOffsetMs = 0;
    for(int i = 0; i < VnrRW::SEND_TIME_LEN; i++)
    {
        recordTimeOffsetMs = (recordTimeOffsetMs << 8) |
                             (unsigned char)headerBuf[VnrRW::RECORD_TIME_OFFSET + i];
    }
    recordHeader.recordTimeOffsetMs = recordTimeOffsetMs;

    return recordHeader;
}


