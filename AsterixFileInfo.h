#ifndef ASTERIXFILEINFO_H
#define ASTERIXFILEINFO_H

#include <QDateTime>
#include <QByteArray>
#include <QList>

namespace AsterixFileInfo
{
    using RecordTime = QDateTime;
    using RecordData = QByteArray;
    using RecordDuration = qint64;
    using RecordCategoriesList = QList<int>;

    using FileDataInfo = std::tuple<RecordTime, RecordDuration, RecordCategoriesList>;
}

#endif // ASTERIXFILEINFO_H
