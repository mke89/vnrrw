#include <QCoreApplication>
#include <QDebug>
#include "DataIOLib.h"
#include "VnrWriter.h"
#include "VnrReader.h"
#include "PcapReader.h"
#include "CommonAsterixReader.h"

int main()
{
    //=================================
    /*
    QString datFile("/home/sintez/workspace/48/21/111.dat");
    const QList<QPair<QDateTime, QByteArray>> &rawData = DataIOLib::readFile(datFile);
    qDebug() << "Size: " << rawData.size();
    QList<VnrRW::RecordType> records;
    for(const QPair<QDateTime, QByteArray> &rawRecord: rawData)
    {
        records << std::make_tuple(rawRecord.first, rawRecord.second, QHostAddress("127.0.0.1"), 55555);
    }

    VnrWriter vnrFile("/home/sintez/workspace/48/21/111.vnr", "123456");
    vnrFile.write(records);
    vnrFile.close();
    */

    //=================================
    /*
    VnrReader vnrReader("/home/sintez/workspace/48/21/111.vnr");
    qDebug() << "Description: " << vnrReader.getDescription();
    qDebug() << "Description size: " << QString::number(vnrReader.getDescriptionSize());
    qDebug() << "Initial time: " << vnrReader.getInitialTime();

    QList<VnrRW::RecordType> records = vnrReader.readAll();
    qDebug() << "Records count: " << records.size();
    */

    //=================================
    /*
    PcapReader pcapReader("/home/sintez/workspace/NTC/PCAP/KMS_Sopka_25.01.21.pcap", QDateTime::currentDateTime());
    //QDateTime recordTime;
    //QByteArray recordData;
    //QHostAddress srcIp;
    //unsigned int srcPort;
    //pcapToDatReader.readRecord(recordTime, recordData, srcIp, srcPort);

    QList<VnrRW::RecordType> records = pcapReader.readAll();
    qDebug() << "Records count: " << records.size();
    */

    //=================================
    /*
    AsterixFileInfo::FileDataInfo pcapFileInfo = PcapReader::getFileDataInfo("/home/sintez/workspace/NTC/PCAP/KMS_Sopka_25.01.21.pcap");
    qDebug() << "Record time: " << std::get<0>(pcapFileInfo).toString();
    qDebug() << "Record duration (secs): " << std::get<1>(pcapFileInfo) / 1000;
    qDebug() << "Record categories: " << std::get<2>(pcapFileInfo);
    */

    //=================================
    AsterixFileInfo::FileDataInfo pcapFileInfo = VnrReader::getFileDataInfo("/home/sintez/dumps/2021/08/25/udpstream_from_54321_16-00.vnr");
    qDebug() << "Record time: " << std::get<0>(pcapFileInfo).toString();
    qDebug() << "Record duration (secs): " << std::get<1>(pcapFileInfo) / 1000;
    qDebug() << "Record categories: " << std::get<2>(pcapFileInfo);

    const QList<QPair<QDateTime, QByteArray>> &vnrRecords = CommonAsterixReader::readFile("/home/sintez/dumps/2021/08/25/udpstream_from_54321_16-00.vnr");
    qDebug() << "VNR records count: " << vnrRecords.size();

    const QList<QPair<QDateTime, QByteArray>> &pcapRecords = CommonAsterixReader::readFile("/home/sintez/workspace/NTC/PCAP/KMS_Sopka_25.01.21.pcap");
    qDebug() << "PCAP records count: " << pcapRecords.size();
}
