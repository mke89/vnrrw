#include "CommonAsterixReader.h"
#include <QFileInfo>
#include <QDir>

CommonAsterixReader::CommonAsterixReader(QString fileName):
    fileName(fileName),
    fileType(AsterixFileType::UNDEFINED),
    recordTimeshift(0)
{
    fileType = defineFileType(fileName);
    open();
}

CommonAsterixReader::CommonAsterixReader(QString fileName, int timeShift_ms):
    fileName(fileName),
    fileType(AsterixFileType::UNDEFINED),
    recordTimeshift(timeShift_ms)
{
    fileType = defineFileType(fileName);
    open();
}
CommonAsterixReader::~CommonAsterixReader()
{
    close();
}

AsterixFileType::FileType CommonAsterixReader::defineFileType(QString fileName)
{
    if(fileName.endsWith(".dat"))
    {
        return AsterixFileType::DAT;
    }

    if(fileName.endsWith(".vnr"))
    {
        return AsterixFileType::VNR;
    }

    if(fileName.endsWith(".pcap"))
    {
        return AsterixFileType::PCAP;
    }

    return AsterixFileType::UNDEFINED;
}

void CommonAsterixReader::setFile(QString fileName)
{
    this->fileName = fileName;
    fileType = defineFileType(fileName);
}

bool CommonAsterixReader::fileOpened()
{
    if(fileType == AsterixFileType::VNR)
    {
        return vnrReader.fileOpened();
    }

    if(fileType == AsterixFileType::PCAP)
    {
        return pcapReader.fileOpened();
    }

    return false;
}

bool CommonAsterixReader::open()
{
    if(fileType == AsterixFileType::VNR)
    {
        vnrReader.setFile(fileName);
        return vnrReader.open();
    }

    if(fileType == AsterixFileType::PCAP)
    {
        pcapReader.setFile(fileName);
        return pcapReader.open();
    }

    if(fileType == AsterixFileType::DAT)
    {
        return true;
    }

    return false;
}

void CommonAsterixReader::close()
{
    if(fileType == AsterixFileType::VNR)
    {
        vnrReader.close();
        return;
    }

    if(fileType == AsterixFileType::PCAP)
    {
        pcapReader.close();
        return;
    }
}

QString CommonAsterixReader::getDescription()
{
    return vnrReader.getDescription();
}

quint32 CommonAsterixReader::getDescriptionSize()
{
    return vnrReader.getDescriptionSize();
}

QDateTime CommonAsterixReader::getInitialTime()
{
    return vnrReader.getInitialTime();
}

void CommonAsterixReader::setStartMarkers(char first, char second)
{
    pcapReader.setStartMarkers(first, second);
}

void CommonAsterixReader::setEndMarkers(char first, char second)
{
    pcapReader.setEndMarkers(first, second);
}

QPair<char, char> CommonAsterixReader::getStartMarkers()
{
    return pcapReader.getStartMarkers();
}

QPair<char, char> CommonAsterixReader::getEndMarkers()
{
    return pcapReader.getEndMarkers();
}

QList<VnrRW::RecordType> CommonAsterixReader::readAll(int cutFirstBytes)
{
    if(fileType == AsterixFileType::VNR)
    {
        return vnrReader.readAll();
    }

    if(fileType == AsterixFileType::PCAP)
    {
        return pcapReader.readAll();
    }

    if(fileType == AsterixFileType::DAT)
    {
        const QList<QPair<QDateTime, QByteArray>> &recordsList = DataIOLib::readFile(fileName, recordTimeshift, cutFirstBytes);
        QList<VnrRW::RecordType> records;

        for(const QPair<QDateTime, QByteArray> &record: recordsList)
        {
            records << std::make_tuple(record.first, record.second, QHostAddress("0.0.0.0"), 0);
        }

        return records;
    }

    return QList<VnrRW::RecordType>();
}

AsterixFileInfo::FileDataInfo CommonAsterixReader::getFileDataInfo(QString fileName, int recordsCount, int cutFirstBytes)
{
    const QString &lowerFileName = fileName.toLower();
    if(lowerFileName.endsWith(".dat"))
    {
        return DataIOLib::getFileDataInfo(fileName, recordsCount, cutFirstBytes);
    }
    else if(lowerFileName.endsWith(".vnr"))
    {
        return VnrReader::getFileDataInfo(fileName, recordsCount);
    }
    else if(lowerFileName.endsWith(".pcap"))
    {
        return PcapReader::getFileDataInfo(fileName, recordsCount);
    }

    return AsterixFileInfo::FileDataInfo();
}

QList<QPair<QDateTime, QByteArray>> CommonAsterixReader::readDir(QString dir, int timeShift_ms)
{
    QFileInfo fileInfo(dir);

    if(fileInfo.isFile())
    {
        return readFile(dir, timeShift_ms);
    }

    if(!fileInfo.isDir())
    {
        return QList<QPair<QDateTime, QByteArray>>();
    }

    QList<QPair<QDateTime, QByteArray>> data;
    QDir directory(dir);


    for(QFileInfo &fileInfo: directory.entryInfoList(QStringList() << "*", QDir::AllEntries | QDir::NoDotAndDotDot))
    {
        qDebug() << fileInfo.absoluteFilePath();
        if(fileInfo.isDir())
        {
            data.append(readDir(fileInfo.absoluteFilePath(), timeShift_ms));
        }
        else if(fileInfo.isFile())
        {
            data.append(readFile(fileInfo.absoluteFilePath(), timeShift_ms));
        }
    }

    return data;
}

QList<QPair<QDateTime, QByteArray>> CommonAsterixReader::readFile(QString file, int timeShift_ms)
{
    const QString &lowerFileName = file.toLower();
    if(lowerFileName.endsWith(".dat"))
    {
        return DataIOLib::readFile(file, timeShift_ms);
    }
    else if(lowerFileName.endsWith(".vnr"))
    {
        return VnrReader::readFile(file);
    }
    else if(lowerFileName.endsWith(".pcap"))
    {
        return PcapReader::readFile(file);
    }

    return QList<QPair<QDateTime, QByteArray>>();
}





