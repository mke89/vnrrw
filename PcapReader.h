#ifndef PCAPTODATREADER_H
#define PCAPTODATREADER_H

#include <QString>
#include <QDateTime>
#include <QHostAddress>
#include "VnrRWTypes.h"

class PcapReader
{
    private:
        typedef enum
        {
            START_MARKER_FIRST  = 0x10,
            START_MARKER_ECOND  = 0x02,
            END_MARKER_FIRST    = 0x10,
            END_MARKER_ECOND    = 0x03,
        } DataMarkers_t;

        typedef enum
        {
            NETWORK_TYPE_UNKNOWN    = 0,
            NETWORK_TYPE_ETHERNET   = 1,
            NETWORK_TYPE_LINUX      = 2,
        } NetworkType_t;

        typedef struct
        {
            bool pcapInverted;
            NetworkType_t pcapNetworkType;
        } PcapServiceData_t;

    public:
        typedef struct PcapSettings
        {
            char startFirstMarker;
            char startSecondMarker;
            char endFirstMarker;
            char endSecondMarker;

            PcapSettings():
                startFirstMarker(START_MARKER_FIRST),
                startSecondMarker(START_MARKER_ECOND),
                endFirstMarker(END_MARKER_FIRST),
                endSecondMarker(END_MARKER_ECOND)
            {
            }
        } PcapSettings_t;

        PcapReader();
        PcapReader(QString file, QDateTime startDateTime = QDateTime());
        ~PcapReader();

        void setFile(QString file);
        bool fileOpened();
        bool open();
        void close();
        QList<VnrRW::RecordType> readAll();
        static AsterixFileInfo::FileDataInfo getFileDataInfo(QString fileName, int stringsCount = 100,
                                                             PcapSettings_t settings = PcapSettings_t());
        static QList<QPair<QDateTime, QByteArray>> readFile(QString file, PcapSettings_t pcapSettings = PcapSettings_t());
        void setStartMarkers(char first, char second);
        void setEndMarkers(char first, char second);
        QPair<char, char> getStartMarkers();
        QPair<char, char> getEndMarkers();

    private:
        FILE *pcapFileHandler;
        PcapServiceData_t pcapServiceData;
        QString fileName;
        QDateTime startDateTime;
        PcapSettings_t pcapSettings;

        static uint32_t ConvertInt(uint32_t in);
        static uint16_t ConvertShort(uint16_t in);
        static QList<VnrRW::RecordType> readRecord(FILE *fileHandler, const PcapSettings_t &userSettings,
                                                                      const PcapServiceData_t &internalSettings);
        static bool openFile(QString fileName, FILE **fileHandler, PcapServiceData_t *serviceData, QDateTime *startDateTime);
        static void clearResources(FILE **fileHandler, QDateTime *startDateTime);
        static AsterixFileInfo::RecordTime getLastRecordTime(FILE **fileHandler, bool pcapInverted);

};

#endif // PCAPTODATREADER_H
